package main.scala

/**
 * Created by Dziabikhina_TS on 07.08.2015.
 */
class Recursion {

  var res = 1;

  def pow(base: Double, degree: Double): Double = {

      if (degree > 0 && degree % 2 == 0) res * pow(pow(base, degree / 2), 2)
      else if (degree > 0 && degree % 2 != 0) res * base * pow(base, degree - 1)
      else if (degree < 0) res * 1 / pow(base, -degree)
      else {
        res;
      }
  }
}


